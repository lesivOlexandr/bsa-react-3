import React from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import classNames from 'classnames';
import { useHistory } from 'react-router-dom';
import { Message } from '../../../../types/message';
import { User } from '../../../../types/user';
import { formatDate } from '../../../../helpers/date-helpers';
import { setEditedMessage, deleteMessage, updateMessage } from '../../actions';
import { AppState } from '../../../../types/app-state';

import './index.css';

interface Props {
    message: Message;
    user: User;
    setEditedMessage: (message: Message | null) => void;
    deleteMessage: (id: string) => void;
    updateMessage: ({id, message}: {id: string, message: Message}) => void;
}

const MessageComponent: React.FC<Props> = (props) => {
    const history = useHistory();

    const { message, user } = props;
    const isMessageOfCurrentUser: boolean = message.userId === user.id;
    const isLiked: boolean = message.likedBy.includes(user.id);

    const onClickLike = () => {
        if (isMessageOfCurrentUser) {
            alert('You can\'t like your own post');
            return;
        }
        let messageData: Message = null!;
        if (isLiked) {
            messageData = {
                ...message,
                likedBy: message.likedBy.filter((userId: string) => user.id !== userId) 
            };
        }
        if (!isLiked) {
            messageData = {
                ...message,
                likedBy: [...message.likedBy, user.id]
            };
        }
        props.updateMessage({id: message.id, message: messageData});
    };

    const onClickDelete = () => {
        props.deleteMessage(message.id);
    };
    const onClickEdit = () => {
        history.push(`/message/${message.id}`);
    };

    return (
        <div className={classNames({
            'chat-body__chat-message': true,
            'chat-body__chat-message_user-current': isMessageOfCurrentUser
        })}>
            {!isMessageOfCurrentUser &&
                <div className="chat-message__user-avatar-wrapper">
                    <img className="chat-message__user-avatar" src={message.avatar ? message.avatar: '/user.png'} alt="" />
                </div>
            }
            <div className="chat-message__text-wrapper">
                <p className="chat-message__text">
                    { message.text }
                </p>
                <div className="chat-message__message-meta">
                    <div className="message-meta__message-icons">
                        <div className={classNames({
                            'message-icons__likes-block': true,
                            'message-meta__like-icon_owner-current-user': isMessageOfCurrentUser,
                            'message-meta__like-icon_state-active': isLiked
                        })}>
                            <span className="likes-block__likes-count">
                                {message.likedBy.length}
                            </span>
                            <span 
                                onClick={onClickLike}
                                className={classNames({
                                    'material-icons message-meta__like-icon': true,
                                    'message-icon': true
                                })}>
                                thumb_up
                            </span>
                        </div>
                        {isMessageOfCurrentUser &&
                            <>
                                <span 
                                    className="material-icons message-icon hidden-until"
                                    onClick={onClickEdit}
                                >
                                    settings
                                </span>
                                <span 
                                    className="material-icons message-icon hidden-until"
                                    onClick={onClickDelete}
                                >
                                    delete
                                </span>
                            </>
                        }
                    </div>
                    <div className="message-meta__time-send">
                        {message.editedAt 
                            ? 'Edited at ' + formatDate(new Date(message.editedAt))
                            : 'Created at ' + formatDate(new Date(message.createdAt)) }
                    </div>
                </div>
            </div>
        </div>
    );
};

(MessageComponent as any).propTypes = {
    message: propTypes.object.isRequired,
    user: propTypes.object.isRequired,
    setEditedMessage: propTypes.func.isRequired,
    deleteMessage: propTypes.func.isRequired,
    updateMessage: propTypes.func.isRequired
};

const mapDispatchToProps = {
    setEditedMessage,
    updateMessage,
    deleteMessage
};

const mapStateToProps = (state: AppState) => ({
    user: state.user!
});

export default connect(mapStateToProps, mapDispatchToProps)(MessageComponent);
