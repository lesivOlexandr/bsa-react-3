import React, { FormEvent, ChangeEvent, useState } from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { addMessage } from '../../actions';
import { User } from '../../../../types/user';
import { AppState } from '../../../../types/app-state';

import './index.css';

interface Props {
    user: User,
    addMessage: ({ text, user }: { text: string, user: User }) => void;
}

const MessageInput: React.FC<Props> = (props) => {
    let [messageText, setMessage] = useState<string>();

    const onSubmit = (e: FormEvent) => {
        e.preventDefault();
        if (messageText) {
            props.addMessage({ text: messageText, user: props.user });
            setMessage('');
        }
    };

    const onChange = (e: ChangeEvent<HTMLTextAreaElement>) => setMessage(e.target.value);

    return (
        <div className="chat__message-input-wrapper">
            <form className="chat__message-input-form" onSubmit={onSubmit}>
                <textarea className="message-input-form__message-input" placeholder="Message" value={messageText} onChange={onChange} />
                <button className="message-input-form__submit-button" type="submit">
                    <span className="material-icons submit-button__icon">publish</span>
                    <span>Send</span>
                </button>
            </form>
        </div>
    );
};

(MessageInput as any).propTypes = {
    user: propTypes.object.isRequired,
    addMessage: propTypes.func.isRequired
};

const mapStateToProps = (state: AppState) => {
    return {
        user: state.user!
    };
};

const mapDispatchToProps = {
    addMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
