import { firestore } from 'firebase';

export type QuerySnapshot<T = DocumentData> = firestore.QuerySnapshot<T>;
export type DocumentData = firestore.DocumentData;
export type DocumentReference = firestore.DocumentReference;
export type QueryDocumentSnapshot<T = DocumentData> = firestore.QueryDocumentSnapshot<T>;