export class Message {
    constructor(
        public id: string,
        public text: string,
        public userId: string,
        public user: string,
        public avatar: string | null,
        public createdAt: number = Date.now(),
        public editedAt: number | null = null,
        public likedBy: string[]
    ){}
}

export class EmptyMessage extends Message {
    constructor(){
        super('', '', '', '', '', Date.now(), null, []);
    }
}

