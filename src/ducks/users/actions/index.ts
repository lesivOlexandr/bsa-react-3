import { makeActionHandler } from '../../../helpers/data-handle-helpers';
import { usersActionTypes } from './action-types';
import { UserCredentials, User } from '../../../types/user';
import { actionTypes } from '../../../actions/action-types';

export const getUsers = makeActionHandler<null>(actionTypes.getUsers);
export const createUser = makeActionHandler<UserCredentials>(usersActionTypes.addUser);
export const updateUser = makeActionHandler<User>(usersActionTypes.updateUser);
export const setEditedUser = makeActionHandler<User>(usersActionTypes.setEditedUser);
export const deleteUser = makeActionHandler<string>(usersActionTypes.deleteUser);
export const fetchUser = makeActionHandler<string>(usersActionTypes.getUser);