import React, { ChangeEvent } from 'react';
import propTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { UserCredentials, User, userTypes } from '../../../../types/user';
import { authenticate } from '../../actions';
import { AppState } from '../../../../types/app-state';

import './index.css';

interface Props {
    authenticate: (data: UserCredentials) => void;
    user: User | null;
    isLoading: boolean;
}

const LoginComponent = (props: Props) => {
    const userData: UserCredentials = {
        name: '', 
        password: '' 
    };
    const onLogin = async () => {
        props.authenticate({...userData});
    };

    const onChange = (event: ChangeEvent<HTMLInputElement>) => {
        const fieldName: string = event.target.name;
        const fieldValue: string = event.target.value;

        userData[fieldName as keyof UserCredentials] = fieldValue;
    };

    if (props.user?.type === userTypes.admin) {
        return <Redirect to="/users" />;
    }

    if (props.user?.type === userTypes.basic){
        return <Redirect to="/chat" />;
    }

    return (
        <div className="login-page">
            <form className="login-page__login-form">
                <input
                    className="login-form__item login-form__field"
                    name="name"
                    placeholder="Username"
                    onChange={onChange} 
                />
                <input
                    className="login-form__item login-form__field"
                    name="password"
                    type="password"
                    placeholder="Password"
                    onChange={onChange}
                />
                <div className="submit-buttons-wrapper login-form__item">
                    <button className="login-form__submit-button" type="button" onClick={onLogin}>Login</button>
                </div>
                <p>
                    Example credentials: 
                    Username: Ben, 
                    Password: password |
                    Username: admin
                    Password: admin
                </p>
            </form>
        </div>
    );
};

(LoginComponent as any).propTypes = {
    authenticate: propTypes.func.isRequired,
    user: propTypes.object,
    isLoading: propTypes.bool
};

const mapStateToProps = (state: AppState) => ({
    user: state.user,
    isLoading: state.isLoading
});

const mapDispatchToProps = {
    authenticate
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent);
