import React, { ChangeEvent, FormEvent } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import propTypes from 'prop-types';
import { User, EmptyUser } from '../../../../types/user';
import { createUser, fetchUser, updateUser, setEditedUser } from '../../actions';
import { AppState } from '../../../../types/app-state';

import './index.css';

interface Props extends RouteComponentProps<{id?: string}> {
    editedUser: User;
    createUser: (data: User) => void;
    fetchUser: (id: string) => void;
    updateUser: (data: User) => void;
    setEditedUser: (data: User) => void;
}

class EditUserModal extends React.Component<Props, User> {
    constructor(props: Props){
        super(props);
        this.state = props.editedUser;
        this.onSave = this.onSave.bind(this);
        this.onFormChange = this.onFormChange.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    onFormChange(e: ChangeEvent<HTMLInputElement>) {
        const fieldName: string = e.target.name;
        const fieldValue: string = e.target.value;

        this.setState({
            ...this.state,
            [fieldName]: fieldValue
        });
    }

    onSave(e: FormEvent) {
        e.preventDefault();
        if (!this.state.name || !this.state.password) {
            alert('You should provide username and password');
            return;
        }

        if (!this.state.id) {
            this.props.createUser({...this.state});
        }
        if (this.state.id) {
            this.props.updateUser({...this.state});
        }
        this.props.setEditedUser(new EmptyUser());
        this.props.history.push('/users');
    }

    onCancel(e: FormEvent) {
        e.preventDefault();
        this.props.setEditedUser(new EmptyUser());
        this.props.history.push('/users');
    }

    componentDidMount(){
        const id: string | undefined = this.props.match.params.id;
        if (id) {
            this.props.fetchUser(id);
        }
    }

    static getDerivedStateFromProps(nextProps: Props, prevState: User) {
        const editedUser: User = nextProps.editedUser;

        if (editedUser.id !== prevState.id && nextProps.match.params.id) {
            return {
                ...nextProps.editedUser
            };
        } else {
            return null;
        }
    }

    render(){
        return (
            <div className="user-modal__backdrop">
                <div className="user-modal__container">
                    <form className="user-modal__user-form" onSubmit={this.onSave} onReset={this.onCancel}>
                        <input 
                            className="user-form__input"
                            type="text"
                            name="name"
                            placeholder="username"
                            value={this.state.name}
                            onChange={this.onFormChange}
                        />
                        <input 
                            className="user-form__input"
                            type="password"
                            name="password"
                            placeholder="password"
                            value={this.state.password}
                            onChange={this.onFormChange}
                        />
                        <div className="user-form__buttons-container">
                            <button type="submit" className="user-form__button user-form__submit-button">Save</button>
                            <button type="reset" className="user-form__button user-form__cancel-button">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

(EditUserModal as any).propTypes = {
    editedUser: propTypes.object.isRequired,
    createUser: propTypes.func.isRequired,
    fetchUser: propTypes.func.isRequired,
    updateUser: propTypes.func.isRequired,
    setEditedUser: propTypes.func.isRequired
};

const mapStateToProps = (state: AppState) => ({
    editedUser: state.editedUser
});

const mapDispatchToProps = {
    createUser,
    fetchUser,
    updateUser,
    setEditedUser
};

export default  connect(mapStateToProps, mapDispatchToProps)(EditUserModal);
