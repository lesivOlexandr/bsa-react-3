import { v4 as uuid } from 'uuid';
import { actionTypes } from '../actions/action-types';

export const genId = (): string => uuid();

export const makeActionHandler = <T>(actionType: actionTypes) => {
    return (data?: T) => ({
        type: actionType,
        payload: data
    });
};
