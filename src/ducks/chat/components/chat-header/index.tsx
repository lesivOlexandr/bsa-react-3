import React from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { Message } from '../../../../types/message';
import { User } from '../../../../types/user';
import { formatDate } from '../../../../helpers/date-helpers';
import { AppState } from '../../../../types/app-state';

import './index.css';

interface Props {
    messages: Message[],
    users: User[]
}

const ChatHeader: React.FC<Props> = (props) => {
    const usersCount: number = props.users.length;
    const messagesCount: number = props.messages.length;
    let lastMessageSendTime: Date | null = null;
    for (const message of props.messages) {
        const messageDate: Date = new Date(message.createdAt);
        if (lastMessageSendTime === null || messageDate > lastMessageSendTime) {
            lastMessageSendTime = messageDate;
        }
    }

    return (
        <div className="chat__chat-header">
            <div className="chat-header__general-inf">
                <h2 className="chat-header__name chat-header__general-inf-item">My Chat</h2>
                <div className="chat-header__users-count chat-header__general-inf-item">
                    <span
                        className="general-inf__users-count"
                    >
                        {usersCount} Participants
                    </span>
                </div>
                <div className="chat-header__messages-count chat-header__general-inf-item">
                    {messagesCount} messages
                </div>
            </div>
            <div className="char-header__date">
                {lastMessageSendTime ? 'Last message at ' + formatDate(lastMessageSendTime): '-' }
            </div>
        </div>
    );
};

(ChatHeader as any).propTypes = {
    users: propTypes.arrayOf(propTypes.object).isRequired,
    messages: propTypes.arrayOf(propTypes.object).isRequired
};

const mapStateToProps = (state: AppState) => {
    return {
        users: state.users,
        messages: state.messages
    };
};

export default connect(mapStateToProps)(ChatHeader);
