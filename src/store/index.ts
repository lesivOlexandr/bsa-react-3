import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware, { SagaMiddleware } from 'redux-saga';
import rootReducer from '../reducers/reducers';
import rootSaga from '../sagas/index';

export default function configureStore (){
    const sagaMiddleware: SagaMiddleware<object> = createSagaMiddleware();
    const store = createStore(
        rootReducer,
        applyMiddleware(sagaMiddleware)
    );

    sagaMiddleware.run(rootSaga);

    return store;
}