import { all } from 'redux-saga/effects';
import usersSagas from '../ducks/users/sagas';
import authSagas from '../ducks/auth/sagas';
import chatSagas from '../ducks/chat/sagas';

export default function* rootSaga(){
    yield all([
        usersSagas(),
        authSagas(),
        chatSagas()
    ]);
}
