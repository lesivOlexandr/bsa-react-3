import { makeActionHandler } from '../../../helpers/data-handle-helpers';
import { UserCredentials } from '../../../types/user';
import { actionTypes } from '../../../actions/action-types';

export const authenticate = makeActionHandler<UserCredentials>(actionTypes.getCurrentUser);

export const setIsLoading = makeActionHandler<boolean>(actionTypes.setLoadingState);