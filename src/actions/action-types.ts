import { usersActionTypes } from '../ducks/users/actions/action-types';
import { authActionTypes } from '../ducks/auth/actions/action-types';
import { chatActionTypes } from '../ducks/chat/actions/action-types';

export const actionTypes = {
    ...usersActionTypes,
    ...authActionTypes,
    ...chatActionTypes
};

export type actionTypes = usersActionTypes | authActionTypes | chatActionTypes;
