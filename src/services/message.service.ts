import { dataStorageProvider } from '../storage/firebase';
import { Message } from '../types/message';
import { User } from '../types/user';
import { DocumentData, QuerySnapshot, DocumentReference, QueryDocumentSnapshot } from '../types/firestore';
import { genId } from '../helpers/data-handle-helpers';

export class MessageService {

    public async getMessages(): Promise<Message[]> {
        const docs: QuerySnapshot<DocumentData> = await dataStorageProvider
            .collection('messages')
            .get();
        const messages: Message[] = [];
        docs.forEach(doc => messages.push(doc.data() as Message));
        return messages;
    }

    public async getMessage(id: string): Promise<Message> {
        const response: QuerySnapshot<DocumentData> = await dataStorageProvider
            .collection('messages')
            .where('id', '==', id)
            .limit(1)
            .get();
        
        if (response.size !== 1) {
            throw new Error('messages with given id not found');
        }

        const doc: QueryDocumentSnapshot<DocumentData> = response.docs[0];
        return doc.data() as Message;
    }

    public async addMessage(text: string, user: User, avatar?: string): Promise<Message> {
        const messageData: Message = {
            id: genId(),
            text,
            userId: user.id,
            user: user.name,
            avatar: avatar || null,
            createdAt: Date.now(),
            editedAt: null,
            likedBy: []
        };
        const messageDoc: DocumentReference = await dataStorageProvider
            .collection('messages')
            .add(messageData);
        const message: Message = (await messageDoc.get()).data() as Message;
        return message;
    }

    public async updateMessage(id: string, message: Message): Promise<void> {
        const response: QuerySnapshot<DocumentData> = await dataStorageProvider
            .collection('messages')
            .where('id', '==', id)
            .limit(1)
            .get();
        if (response.size === 1) {
            const doc = response.docs[0];
            await doc.ref.update(message);
        }
    }

    public async deleteMessage(id: string): Promise<Message> {
        const response: QuerySnapshot<DocumentData> = await dataStorageProvider
            .collection('messages')
            .where('id', '==', id)
            .limit(1)
            .get();
    
        let message: Message = null!;
        if (response.size === 1) {
            const messageDoc: QueryDocumentSnapshot<DocumentData> = response.docs[0];
            message = messageDoc.data() as Message;
            await messageDoc.ref.delete();
        }
        return message;
    }
}

export const messageService = new MessageService();
