import moment from 'moment';

export const second = 1000;
export const minute = second * 60;
export const hour = minute * 60;
export const day = hour * 24;

export const formatDate = (date: Date) => {
    if (Date.now() - date.valueOf() < day) {
        return moment(date).format('hh:mm');
    }
    return moment(date).format('DD/MM/YYYY, hh:mm');
};
