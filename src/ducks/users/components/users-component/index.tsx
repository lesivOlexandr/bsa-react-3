import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect, useHistory, Link } from 'react-router-dom';
import { User, userTypes } from '../../../../types/user';
import { AppState } from '../../../../types/app-state';
import { deleteUser } from '../../actions';

import './index.css';

interface Props {
    user: User | null;
    users: User[];
    deleteUser: (id: string) => void;
}

const UsersPageComponent: React.FC<Props> = (props: Props) => {
    const users: User[] = props.users.filter(user => user.type !== userTypes.admin);
    const history = useHistory();

    const onAdd = () => {
        history.push('/user');
    };

    const onEdit = (id: string) => {
        history.push(`/user/${id}`);
    };

    const onDelete = (id: string) => {
        props.deleteUser(id);
    };

    if (props.user?.type !== userTypes.admin) {
        return <Redirect to='/' />;
    }

    return (
        <div className="users-page">
            <div className="users-page__add-user-button-wrapper">
                <button className="user-page__add-user-button" onClick={onAdd}>
                    Add User
                </button>
            </div>
            <div className="users-page__user-list">
                {users.map(user => (
                    <div className="users-page__user-block" key={user.id}>
                        <div className="user-block__username user-block__item">
                            {user.name}
                        </div>
                        <div>
                            <button 
                                className="user-block-button user-block__item"
                                onClick={() => onEdit(user.id)}
                            >
                                Edit
                            </button>
                            <button 
                                className="user-block-button user-block__item" 
                                onClick={() => onDelete(user.id)}
                            >Delete</button>
                        </div>
                    </div>
                ))}
            </div>
            <div className="users-page__go-to-chat">
                <Link to="/chat">Go to chat</Link>
            </div>
        </div>
    );
};

(UsersPageComponent as any).propTypes = {
    user: propTypes.object,
    users: propTypes.arrayOf(propTypes.object).isRequired,
    deleteUser: propTypes.func.isRequired
};

const mapStateToProps = (state: AppState) => ({
    user: state.user,
    users: state.users
});

const mapDispatchToProps = {
    deleteUser
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersPageComponent);
