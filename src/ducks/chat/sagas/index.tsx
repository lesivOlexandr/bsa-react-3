import { put, call, takeEvery, all } from 'redux-saga/effects';
import { Message } from '../../../types/message';
import { messageService } from '../../../services/message.service';
import { actionTypes } from '../../../actions/action-types';
import { Action } from '../../../types/action';
import { User } from '../../../types/user';

function* fetchMessages() {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const messages: Message[] = yield call(messageService.getMessages);
        yield put({ type: actionTypes.setLoadingState, payload: true });
        yield put({ type: actionTypes.getMessagesSuccess, payload: messages });
    } catch (error) {
        alert(error.message);
        yield put({ type: actionTypes.getMessagesFailure });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

export function* watchFetchMessages() {
    yield takeEvery(actionTypes.getMessages, fetchMessages);
}

function* fetchMessage(action: Action<string>) {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const message: Message = yield call(messageService.getMessage, action.payload);
        yield put({ type: actionTypes.getMessageSuccess, payload: message });
        yield put({ type: actionTypes.getMessages });
    } catch (error) {
        alert(error.message);
        yield put({ type: actionTypes.getMessageFailure });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

function* watchFetchMessage(){
    yield takeEvery(actionTypes.getMessage, fetchMessage);
}

function* addMessage(action: Action<{text: string, user: User}>) {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const message: Message = yield call(messageService.addMessage, action.payload.text, action.payload.user);
        yield put({ type: actionTypes.addMessageSuccess, payload: message });
        yield put({ type: actionTypes.getMessages });
    } catch (error) {
        alert(error.message);
        yield put({ type: actionTypes.addMessageFailure, payload: error.message });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

export function* watchAddMessage() {
    yield takeEvery(actionTypes.addMessage, addMessage);
}

function* updateMessage(action: Action<{id: string, message: Message}>) {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const message: Message = yield call(messageService.updateMessage, action.payload.id, action.payload.message);
        yield put({ type: actionTypes.updateMessageSuccess, payload: message });
        yield put({ type: actionTypes.getMessages });
    } catch (error) {
        alert(error.message);
        yield put({ type: actionTypes.updateMessageFailure });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

export function* watchUpdateMessage() {
    yield takeEvery(actionTypes.updateMessage, updateMessage);
}

function* deleteMessage(action: Action<string>) {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const message: Message = yield call(messageService.deleteMessage, action.payload);
        yield put({ type: actionTypes.deleteMessageSuccess, payload: message });
        yield put({ type: actionTypes.getMessages });
    } catch (error) {
        alert(error.message);
        yield put({ type: actionTypes.deleteUserFailure });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

export function* watchDeleteMessage() {
    yield takeEvery(actionTypes.deleteMessage, deleteMessage);
}

export default function* rootSaga(){
    yield all([
        watchFetchMessages(),
        watchFetchMessage(),
        watchAddMessage(),
        watchUpdateMessage(),
        watchDeleteMessage()
    ]);
}
