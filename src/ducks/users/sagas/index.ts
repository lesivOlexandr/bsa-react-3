import { call, put, takeEvery, all } from 'redux-saga/effects';
import { User, UserCredentials } from '../../../types/user';
import { userService } from '../../../services/users.service';
import { actionTypes } from '../../../actions/action-types';
import { Action } from '../../../types/action';

export function* fetchUsers() {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const users: User[] = yield call(userService.getUsers);
        yield put({ type: actionTypes.getUsersSuccess, payload: users });
    } catch (error) {
        alert(error.message);
        yield put({ type: actionTypes.getUsersFailure, payload: error.message });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

function* watchFetchUsers() {
    yield takeEvery(actionTypes.getUsers, fetchUsers);
}

function* fetchUser(action: Action<string>) {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const user: User = yield call(userService.getUser, action.payload);
        yield put({ type: actionTypes.getUserSuccess, payload: user });
    } catch(error) {
        alert(error.message);
        yield put({ type: actionTypes.getUserFailure });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

export function* watchFetchUser() {
    yield takeEvery(actionTypes.getUser, fetchUser);
}

function* updateUser(action: Action<User>) {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const user: User = yield call(userService.updateUser, action.payload.id, action.payload);
        yield put({ type: actionTypes.updateUserSuccess, payload: user });
        yield put({ type: actionTypes.getUsers });
    } catch (error) {
        alert(error.message);
        yield put({ type: actionTypes.updateUserFailure });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

export function* watchUpdateUser() {
    yield takeEvery(actionTypes.updateUser, updateUser);
}

function* deleteUser(action: Action<string>) {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const user: User = yield call(userService.deleteUser, action.payload);
        yield put({ type: actionTypes.getUsers, payload: user });
    } catch (error) {
        alert(error.message);
        yield put({ type: actionTypes.deleteUserFailure });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

export function* watchDeleteUser(){
    yield takeEvery(actionTypes.deleteUser, deleteUser);
}

export function* createUser(action: Action<UserCredentials>) {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const { name, password } = action.payload;
        const user: User = yield call(userService.addUser, new User(name, password));
        yield put({ type: actionTypes.addUserSuccess, payload: user });
        yield put({ type: actionTypes.getUsers });
    } catch (error) {
        alert(error.message);
        yield put({ type: actionTypes.addUserFailure });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

export function* watchCreateUser() {
    yield takeEvery(actionTypes.addUser, createUser);
}

export default function* usersSaga(){
    yield all([
        watchFetchUsers(),
        watchCreateUser(),
        watchFetchUser(),
        watchUpdateUser(),
        watchDeleteUser(),
    ]);
}
