import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';
import { connect } from 'react-redux';
import { Message } from '../../../../types/message';
import MessageComponent from '../message/message';
import { AppState } from '../../../../types/app-state';

import './index.css';

interface Props {
    messages: Message[]
}

const MessageList: React.FC<Props> = (props) => {
    const dateMessages: {[key: string]: Message[]} = {};

    for (const message of props.messages) {
        const date: Date = new Date(message.createdAt);
        let key = moment(date).format('DD/MM/YYYY');

        if (!dateMessages[key]) {
            dateMessages[key] = [];
        }

        dateMessages[key].push(message);
    }

    const dates: string[] = Object.keys(dateMessages);

    return (
        <div className="chat__chat-body"> 
            {dates.map(key => (
                <div key={key} className="chat-body__group">
                    <div className="chat-body__divided">
                        <div className="chat-body__date">{key}</div>
                    </div>
                    {dateMessages[key].map(message => (
                        <MessageComponent message={message} key={message.id} />
                    ))}
                </div>
            ))}
        </div>
    );
};

(MessageList as any).propTypes = {
    messages: propTypes.arrayOf(propTypes.object).isRequired
};

const mapStateToProps = (state: AppState) => {
    return {
        messages: state.messages
    };
};

export default connect(mapStateToProps)(MessageList);
