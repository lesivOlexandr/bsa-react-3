import { genId } from '../helpers/data-handle-helpers';

export interface UserCredentials {
    name: string,
    password: string
}

export enum userTypes {
    admin = 'admin',
    basic = 'basic'
}

export class User implements UserCredentials {
    constructor(
        public name: string,
        public password: string,
        public type: userTypes = userTypes.basic,
        public id: string = genId()
    ){}
}

export class EmptyUser extends User {
    constructor(){
        super('', '', userTypes.basic, '');
    }
}
