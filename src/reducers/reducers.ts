import { actionTypes } from '../actions/action-types';
import { AppState } from '../types/app-state';
import { Action } from '../types/action';

const initialState: AppState = new AppState();

export default (state: AppState = initialState, action: Action<any>): AppState => {
    switch(action.type) {
    case actionTypes.getUsersSuccess:
        return {
            ...state,
            users: [...action.payload]
        };
    case actionTypes.getCurrentUserSuccess:
        return {
            ...state,
            user: action.payload
        };
    case actionTypes.getUserSuccess:
        return {
            ...state,
            editedUser: action.payload
        };
    case actionTypes.getMessagesSuccess:
        return {
            ...state,
            messages: [...action.payload]
        };
    case actionTypes.getMessageSuccess:
        return {
            ...state,
            editedMessage: action.payload
        };
    case actionTypes.setEditedMessage:
        return {
            ...state,
            editedMessage: action.payload ? { ...action.payload }: null
        };
    case actionTypes.setLoadingState:
        return {
            ...state,
            isLoading: action.payload
        };
    case actionTypes.setEditedUser:
        return {
            ...state,
            editedUser: action.payload
        };
    default:
        return state;
    }
};
