import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import ChatComponent from './ducks/chat/components/chat'; 
import LoginComponent from './ducks/auth/components/login';
import AppHeader from './ducks/app/app-header';
import AppFooter from './ducks/app/app-footer';
import UsersPageComponent from './ducks/users/components/users-component';
import EditUserModal from './ducks/users/components/edit-user-modal';
import EditMessageModal from './ducks/chat/components/edit-message-modal';
import Preloader from './ducks/app/preloader';
import { AppState } from './types/app-state';

import './App.css';

interface Props { 
  isLoading: boolean;
}

const App = (props: Props) => {
    return (
        <div className="app">
            <AppHeader />
            <Switch>
                <Route exact path="/" component={LoginComponent} />
                <Route exact path="/chat" component={ChatComponent} />
                <Route exact path="/users" component={UsersPageComponent} />
                <Route exact path="/user" component={EditUserModal} />
                <Route exact path="/user/:id" component={EditUserModal} />
                <Route exact path="/message/:id" component={EditMessageModal} />
            </Switch>
            <AppFooter />
            {props.isLoading && <Preloader />}
        </div>
    );
};

const mapStateToProps = (state: AppState) => ({
    isLoading: state.isLoading
});

export default connect(mapStateToProps)(App);
