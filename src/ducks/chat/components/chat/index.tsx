import React, { useEffect } from 'react';
import propTypes from 'prop-types';
import MessageList from '../message-list';
import ChatHeader from '../chat-header';
import MessageInput from '../message-input';
import { Message } from '../../../../types/message';
import { AppState } from '../../../../types/app-state';
import { connect } from 'react-redux';
import { setEditedMessage } from '../../actions';
import { User } from '../../../../types/user';
import { Redirect } from 'react-router-dom';

interface Props {
    messages: Message[];
    user: User;
    editedMessage: Message | null;
    setEditedMessage: (message: Message | null) => void;
}

const ChatComponent: React.FC<Props> = (props) => {
    useEffect(() => {
        document.addEventListener('keydown', handleKeyPress);
        return () => document.removeEventListener('keydown', handleKeyPress);
    });

    const handleKeyPress = (e: KeyboardEvent) => {
        if (e.keyCode === 38 && !props.editedMessage) {
            e.preventDefault();
            const lastMessage = props.messages.reverse().find(message => message.userId === props.user.id);
            if (lastMessage) {

                setEditedMessage(lastMessage);
            }
        }
    };

    if (!props.user) {
        return <Redirect to="/"></Redirect>;
    }

    return (
        <div className="chat">
            <ChatHeader />
            <MessageList />
            <MessageInput />
        </div>
    );
};

(ChatComponent as any).propTypes = {
    user: propTypes.object.isRequired,
    messages: propTypes.arrayOf(propTypes.object).isRequired,
    editedMessage: propTypes.object,
    setEditedMessage: propTypes.func.isRequired
};

const mapStateToProps = (state: AppState) => ({
    user: state.user!,
    messages: state.messages,
    editedMessage: state.editedMessage,
});

const mapDispatchToProps = {
    setEditedMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatComponent);
