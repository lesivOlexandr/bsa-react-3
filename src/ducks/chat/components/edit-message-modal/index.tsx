import React, { FormEvent, ChangeEvent } from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { setEditedMessage, getMessage, updateMessage } from '../../actions';
import { Message, EmptyMessage } from '../../../../types/message';
import { AppState } from '../../../../types/app-state';

import './index.css';

interface Props extends RouteComponentProps<{id: string}> {
    editedMessage: Message;
    getMessage: (id: string) => void;
    updateMessage: ({id, message}: {id: string, message: Message}) => void;
    setEditedMessage: (message: Message) => void;
}

class EditMessageModal extends React.Component<Props, Message> {
    constructor(props: Props){
        super(props);
        this.state = props.editedMessage;
        this.onCancel = this.onCancel.bind(this);
        this.onSubmitEdit = this.onSubmitEdit.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
    }

    onCancel = (e: FormEvent) => {
        e.preventDefault();
        this.props.history.push('/chat');
        this.props.setEditedMessage(new EmptyMessage());
    };

    onSubmitEdit = (e: FormEvent) => {
        e.preventDefault();
        if (this.state.text) {
            const editedMessage = {...this.props.editedMessage};
            editedMessage.editedAt = Date.now();
            editedMessage.text = this.state.text;
            this.props.updateMessage({ id: editedMessage.id, message: editedMessage });
            this.props.history.push('/chat');
            this.props.setEditedMessage(new EmptyMessage());
        }
    };

    onTextChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
        const value: string = e.target.value;
        this.setState({...this.state, text: value});
    };

    componentDidMount(){
        const id: string | undefined = this.props.match.params.id;
        if (id) {
            this.props.getMessage(id);
        }
    }

    static getDerivedStateFromProps(nextProps: Props, prevState: Message) {
        const editedMessage: Message = nextProps.editedMessage;
        if (editedMessage.id !== prevState.id && nextProps.match.params.id) {
            return {
                ...nextProps.editedMessage
            };
        } else {
            return null;
        }
    }

    render() {
        return (
            <div className="chat__modal-backdrop">
                <div className="chat__modal-container">
                    <form className="modal-container-wrapper" onReset={this.onCancel} onSubmit={this.onSubmitEdit}>
                        <h3>Input new message text</h3>
                        <textarea 
                            className="modal-container__edit"
                            placeholder="message"
                            value={this.state.text}
                            onChange={this.onTextChange}
                        />
                        <div className="modal-container__buttons">
                            <button className="modal-container__submit-edit-button" type="submit">Save</button>
                            <button className="modal-container__cancel-edit-button" type="reset">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

(EditMessageModal as any).propTypes = {
    editedMessage: propTypes.object.isRequired,
    getMessage: propTypes.func.isRequired,
    setEditedMessage: propTypes.func.isRequired,
    updateMessage: propTypes.func.isRequired
};

const mapStateToProps = (state: AppState) => ({
    editedMessage: state.editedMessage!
});

const mapDispatchToProps = {
    getMessage,
    setEditedMessage,
    updateMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageModal);
