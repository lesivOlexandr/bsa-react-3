import firebase, { firestore } from 'firebase/app';
import 'firebase/firestore';
import { firebaseConfig } from '../config';

firebase.initializeApp(firebaseConfig);
 
export const dataStorageProvider: firestore.Firestore = firebase.firestore();
