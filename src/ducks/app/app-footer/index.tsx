import React from 'react';

import './index.css';

const AppFooter = () => {
    return (
        <div className="app-footer">
            <div className="app-footer__copyright">
                Copyright
            </div>
        </div>
    );
};

export default AppFooter;
