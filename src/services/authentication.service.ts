import { dataStorageProvider } from '../storage/firebase';
import { User } from '../types/user';
import { DocumentData, QuerySnapshot } from '../types/firestore';

export class AuthenticationService {
    public async login(userName: string, password: string): Promise<User> {
        const response: QuerySnapshot<DocumentData> = await dataStorageProvider
            .collection('users')
            .where('name', '==', userName)
            .where('password', '==', password)
            .limit(1)
            .get();
        if (response.size === 1) {
            return response.docs[0].data() as User;
        }
        throw new Error('Username or password is incorrect');
    }
}

export const authenticationService = new AuthenticationService();
