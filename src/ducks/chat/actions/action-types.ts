export enum chatActionTypes {
    getMessages = 'GET_MESSAGES',
    getMessagesSuccess = 'GET_MESSAGES_SUCCESS',
    getMessagesFailure = 'GET_MESSAGES_FAILURE',

    getMessage = 'GET_MESSAGE',
    getMessageSuccess = 'GET_MESSAGE_SUCCESS',
    getMessageFailure = 'GET_MESSAGE_FAILURE',

    addMessage = 'ADD_MESSAGE',
    addMessageSuccess = 'ADD_MESSAGE_SUCCESS',
    addMessageFailure = 'UPDATE_MESSAGE_Failure',

    updateMessage = 'UPDATE_MESSAGE',
    updateMessageSuccess = 'UPDATE_MESSAGE_SUCCESS',
    updateMessageFailure = 'UPDATE_MESSAGE_FAILURE',

    deleteMessage = 'DELETE_MESSAGE',
    deleteMessageSuccess = 'DELETE_MESSAGE_SUCCESS',
    deleteMessageFailure = 'DELETE_MESSAGE_FAILURE',

    setEditedMessage = 'SET_EDITED_MESSAGE'
}