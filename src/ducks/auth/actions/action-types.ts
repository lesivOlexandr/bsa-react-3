export enum authActionTypes {
    getCurrentUser = 'GET_CURRENT_USER',
    getCurrentUserSuccess = 'GET_CURRENT_USER_SUCCESS',
    getCurrentUserFailure = 'GET_CURRENT_USER_FAILURE',
    setLoadingState = 'SET_LOADING'
}
    