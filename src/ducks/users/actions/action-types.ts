export enum usersActionTypes {
    getUsers = 'GET_USERS',
    getUsersSuccess = 'GET_USERS_SUCCESS',
    getUsersFailure = 'GET_USERS_FAILURE',

    getUser = 'GET_USER',
    getUserSuccess = 'GET_USER_SUCCESS',
    getUserFailure = 'GEt_USER_FAILURE',

    addUser = 'ADD_USER',
    addUserSuccess = 'ADD_USER_SUCCESS',
    addUserFailure = 'ADD_USER_FAILURE',

    updateUser = 'UPDATE_USER',
    updateUserSuccess = 'UPDATE_USER_SUCCESS',
    updateUserFailure = 'UPDATE_USER_FAILURE',

    getCurrentUser = 'GET_CURRENT_USER',
    getCurrentUserSuccess = 'GET_CURRENT_USER_SUCCESS',
    getCurrentUserFailure = 'GET_CURRENT_USER_FAILURE',

    deleteUser = 'DELETE_USER',
    deleteUserSuccess = 'DELETE_USER_SUCCESS',
    deleteUserFailure = 'DELETE_USER_FAILURE',

    setEditedUser = 'SET_EDITED_USER'
}
