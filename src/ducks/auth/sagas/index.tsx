import { UserCredentials, User } from '../../../types/user';
import { Action } from '../../../types/action';
import { put, takeEvery, call, all } from 'redux-saga/effects';
import { actionTypes } from '../../../actions/action-types';
import { authenticationService } from '../../../services/authentication.service';

export function* login(action: Action<UserCredentials>) {
    yield put({ type: actionTypes.setLoadingState, payload: true });
    try {
        const { name, password } = action.payload;

        const user: User = yield call(authenticationService.login, name, password);
        yield put({ type: actionTypes.getCurrentUserSuccess, payload: user });
        yield put({ type: actionTypes.getUsers });
        yield put({ type: actionTypes.getMessages });
    } catch (error) {
        alert(error.message);
        yield put({ type: actionTypes.getCurrentUserFailure, payload: error.message });
    }
    yield put({ type: actionTypes.setLoadingState, payload: false });
}

function* watchUserLogin() {
    yield takeEvery(actionTypes.getCurrentUser, login);
}

export default function* usersSaga(){
    yield all([
        watchUserLogin()
    ]);
}
