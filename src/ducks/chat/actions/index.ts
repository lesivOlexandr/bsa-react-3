import { actionTypes } from '../../../actions/action-types';
import { makeActionHandler } from '../../../helpers/data-handle-helpers';
import { User } from '../../../types/user';
import { Message } from '../../../types/message';

export const getMessages = makeActionHandler<null>(actionTypes.getMessages);
export const addMessage = makeActionHandler<{text: string, user: User}>(actionTypes.addMessage);
export const getMessage = makeActionHandler<string>(actionTypes.getMessage);
export const updateMessage = makeActionHandler<{id: string, message: Message}>(actionTypes.updateMessage);
export const deleteMessage = makeActionHandler<string>(actionTypes.deleteMessage);
export const setEditedMessage = makeActionHandler<Message | null>(actionTypes.setEditedMessage);