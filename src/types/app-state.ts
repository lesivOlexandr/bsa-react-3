import { User, EmptyUser } from './user';
import { Message, EmptyMessage } from './message';

export class AppState {
    constructor(
        public users: User[] = [],
        public user: User | null = null,
        public messages: Message[] = [],
        public editedMessage: Message = new EmptyMessage(),
        public editedUser: User = new EmptyUser(),
        public isLoading: boolean = false
    ){}
}
