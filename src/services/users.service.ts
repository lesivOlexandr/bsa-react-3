import { dataStorageProvider } from '../storage/firebase';
import { User } from '../types/user';
import { QuerySnapshot, DocumentData, DocumentReference, QueryDocumentSnapshot } from '../types/firestore';
import { genId } from '../helpers/data-handle-helpers';

export class UserService {
    public async getUsers(): Promise<User[]> {
    // it's still unsafe because anybody can get user password by this way
    // but I guess for demonstrating purposes it's ok
        const userDocs: QuerySnapshot<DocumentData> = await dataStorageProvider
            .collection('users')
            .get();
    
        const users: User[] = [];
        userDocs.forEach(doc => users.push(doc.data() as User));
        return users;
    }

    public async getUser(id: string): Promise<User> {
        const userDocs: QuerySnapshot<DocumentData> = await dataStorageProvider
            .collection('users')
            .where('id', '==', id)
            .limit(1)
            .get();
        
        if (userDocs.size === 1) {
            return userDocs.docs[0].data() as User;
        }

        throw new Error('No users found');
    }

    public async addUser(data: User): Promise<User> {
        data.id = genId();
        const userDoc: DocumentReference = await dataStorageProvider
            .collection('users')
            .add({...data});

        const user: User = (await userDoc.get()).data() as User;
        return user;
    }

    public async updateUser(id: string, data: User): Promise<User> {
        const userDocs: QuerySnapshot<DocumentData> = await dataStorageProvider
            .collection('users')
            .where('id', '==', id)
            .limit(1)
            .get();
        
        if (userDocs.size !== 1) {
            throw new Error('No users matches given id');
        }

        const userDoc: QueryDocumentSnapshot<DocumentData> = userDocs.docs[0];
        await userDoc.ref.update(data);
        return (await userDoc.ref.get()).data() as User;
    }

    public async deleteUser(id: string): Promise<User> {
        const userDocs: QuerySnapshot<DocumentData> = await dataStorageProvider
            .collection('users')
            .where('id', '==', id)
            .limit(1)
            .get();
        
        if (userDocs.size !== 1) {
            throw new Error('No users matches given id');
        }

        const userDoc: QueryDocumentSnapshot<DocumentData> = userDocs.docs[0];
        const user: User = userDoc.data() as User;
        await userDoc.ref.delete();
        return user; 
    }
}

export const userService = new UserService();
